GCC = gcc
C_FLAGS = -Wpedantic -Wall

all : superserver

test : all myfunction.h
	${GCC} -o tcpServer tcpServer.c
	${GCC} -o udpServer udpServer.c
	${GCC} -o tcpClient tcpClient.c
	${GCC} -o udpClient udpClient.c

superserver : superserver.o
	${GCC} -o superserver superserver.o

superserver.o : superserver.c
	${GCC} -c ${C_FLAGS} -o superserver.o superserver.c

.PHONY : clean

clean :
	-rm superserver.o superserver
