#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/socket.h>
#include<sys/time.h>
#include<netinet/in.h>
#include<signal.h>
#include<errno.h>
#include<unistd.h>

//Constants

#define CONFIG_FILE_PATHNAME "ss.txt"
#define MAX_SERVICE_NUMBER 10
#define TCP_PROTO "tcp"
#define UDP_PROTO "udp"
#define WAIT_MODE "wait"
#define NOWAIT_MODE "nowait"
#define TRANSPORT_PROTOCOL_LEN 4
#define MODE_LEN 7
#define PORT_LEN 10
#define PATH_LEN 100
#define NAME_LEN 50
#define BACK_LOG 10 // Maximum queued requests

//Service structure definition goes here
typedef struct service_data{
        char transport_protocol[TRANSPORT_PROTOCOL_LEN];
        char mode[MODE_LEN];
        char port[PORT_LEN];
        char path[PATH_LEN];
        char name[NAME_LEN];
        int sfd;
        int pid;
} service;

// Global variables

int n_services = 0;								// number of running services
service service_array[MAX_SERVICE_NUMBER];		// services array structure
fd_set select_set;								// set for select function

// Function prototype devoted to handle the death of the son process
void handle_signal (int sig);

// Parse the configuration file, filling informations strings
// Returns the number of services parsed or -1 if an error occurred
int parse_config (char *file_path, char **full_path_names, char **names, char **protocols, char **ports, char **modes);

// Handles a single request coming to a service
// Returns 0 if the request has been correctly handled, -1 if an error occurred
int handle_request (service *serv, char **env);

// Returns true if the service passed is wait mode
int is_wait_mode (service serv);

// Returns true if the service passed is tcp based
int is_tcp (service serv);

// Removes endline character from the string if any
void trim_endline (char *s, int length);

int main(int argc, char **argv, char **env) {
	char **full_path_names = malloc (MAX_SERVICE_NUMBER * sizeof (char *));
	char **names = malloc (MAX_SERVICE_NUMBER * sizeof (char *));
	char **protocols = malloc (MAX_SERVICE_NUMBER * sizeof (char *));
	char **ports = malloc (MAX_SERVICE_NUMBER * sizeof (char *));
	char **modes = malloc (MAX_SERVICE_NUMBER * sizeof (char *));
	
	// parsing configured services
	n_services = parse_config (CONFIG_FILE_PATHNAME, full_path_names, names, protocols, ports, modes);

	if (n_services < 0) {
		perror ("error while parsing services");
		exit (EXIT_FAILURE);
	}

	printf("populating service structures...\n");

	// populating service structures
	for (int i = 0; i < n_services && i < MAX_SERVICE_NUMBER; i++) {
		// copying information to structure
		strncpy (service_array[i].transport_protocol, protocols[i], sizeof(char) * TRANSPORT_PROTOCOL_LEN);
		strncpy (service_array[i].mode, modes[i], sizeof(char) * MODE_LEN);
		strncpy (service_array[i].port, ports[i], sizeof(char) * PORT_LEN);
		strncpy (service_array[i].path, full_path_names[i], sizeof(char) * PATH_LEN);
		strncpy (service_array[i].name, names[i], sizeof(char) * NAME_LEN);

		// creating socket (TCP or UDP)
		if (is_tcp (service_array[i])) {
			service_array[i].sfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
		}
		else{
			service_array[i].sfd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		}
		if (service_array[i].sfd < 0) {
			perror ("error on socket creation");
			exit (EXIT_FAILURE);
		}
		
		// binding socket to port
		struct sockaddr_in server_addr;
		server_addr.sin_family = AF_INET;
		server_addr.sin_port = htons (strtol (service_array[i].port, NULL, 10));	// convertion to long
		server_addr.sin_addr.s_addr = INADDR_ANY;

		if (bind (service_array[i].sfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
			perror ("error while binding socket to port");
			exit (EXIT_FAILURE);
		}

		// start listening if TCP
		if (is_tcp (service_array[i])) {
			if (listen (service_array[i].sfd, BACK_LOG) < 0) {
				perror ("error while start listening to socket");
				exit (EXIT_FAILURE);
			}
    	}

    	// deallocating buffers
    	free (full_path_names[i]);
    	free (names[i]);
    	free (protocols[i]);
    	free (ports[i]);
    	free (modes[i]);
	}

	// deallocating pointers to buffers
	free (full_path_names);
	free (names);
	free (protocols);
	free (ports);
	free (modes);

	printf("populating set for select...\n");

	// populating set for select
	FD_ZERO (&select_set);

	for (int i = 0; i < n_services; i++) {
		FD_SET (service_array[i].sfd, &select_set);
	}

	printf("starting to handle signals from children...\n");
	
	// setting handle signals from children
	signal (SIGCHLD, handle_signal);

	// main loop
	while (1) {
		// set for selecting ready sfds
		fd_set ready_set = select_set;
		
		// searching max sfd
		int max_sfd = 0;
		for (int i = 0; i < n_services; i++) {
			if (service_array[i].sfd > max_sfd) {
				max_sfd = service_array[i].sfd;
			}
		}

		printf ("waiting for a request...\n");

		// selecting ready sfd for I/O
		int ready_sfds = select (max_sfd + 1, &ready_set, NULL, NULL, NULL);

		// checking for errors
		if (ready_sfds < 0) {
			if (errno == EINTR) {
				printf ("select interrupted by a signal!\n");
				continue;			// interruption by signalling, ignoring error
			}

			perror ("error on select function");
			exit (EXIT_FAILURE);
		}

		printf ("requests found! number of requests: %d\n", ready_sfds);

		// handling requests from services
		for (int i = 0; i < n_services; i++) {
			service *current_service = service_array + i;

			if (FD_ISSET (current_service->sfd, &ready_set)) {
				// handle current_service
				printf ("handling request for service:(%s)...\n", current_service->name);

				int req = handle_request (current_service, env);

				if (req < 0) {
					perror ("error while handling the request");
					exit (EXIT_FAILURE);
				}

				printf ("request for service (%s) handled\n", current_service->name);
			}
		}

		// sleep to give time to services to read messages
		usleep(10000 * ready_sfds);
	}

	return 0;
}

void handle_signal (int sig) {
	// Call to wait system-call goes here
	int *status = malloc (sizeof (int));
	pid_t child_pid = wait (status);

	printf ("child service has died...\n");
	
	switch (sig) {
		case SIGCHLD : 
			if (*status != 0) {
				errno = *status;
				perror ("child exited with an error!\n");
			}
			free (status);

			// searching for service with child_pid
			for (int i = 0; i < n_services; i++) {
				if (service_array[i].pid == child_pid && is_wait_mode(service_array[i])) {
					// inserting service sfd back in the select set
					FD_SET (service_array[i].sfd, &select_set);
					printf("reinserted service in the set\n");
				}
			}
			
			break;
		default : printf ("signal not known!\n");
			break;
	}
	
	// resetting handle signals from children
	signal (SIGCHLD, handle_signal);
}

void trim_endline (char *s, int length) {
	if (s[length - 1] == '\n')
		s[length - 1] = '\0';
}

int parse_config (char *file_path, char **full_path_names, char **names, char **protocols, char **ports, char **modes) {
	printf ("opening config file (%s)...\n", file_path);

	FILE *conf = fopen (file_path, "r");

	if (conf == NULL) {
		perror ("error while opening the config file");
		return -1;
	}

	int buff_size = 1024;
	char buff[buff_size];

	printf ("parsing services...\n");

	// reading all lines in the config file
	int count;
	for (count = 0; fgets (buff, buff_size, conf) != NULL; count++) {
		// trimming last endline character if any
		trim_endline (buff, strlen (buff));

		// ignoring empty lines
		if (strlen(buff) == 0)
			break;

		// separating service infos
		char *separators = " ";

		char *buff_ptr = buff;

		char *path = strsep (&buff_ptr, separators);
		char *proto = strsep (&buff_ptr, separators);
		char *port = strsep (&buff_ptr, separators);
		char *mode = strsep (&buff_ptr, separators);

		// check for wrong format
		if (strsep (&buff_ptr, separators) != NULL) {
			perror ("too much service information");
			return -1;
		}

		if (path == NULL || proto == NULL || port == NULL || mode == NULL) {
			perror ("parsing error");
			return -1;
		}

		// getting service name from path
		full_path_names[count] = malloc (strlen (path) + 1);
		strncpy (full_path_names[count], path, strlen (path) + 1);

		char *path_separators = "/\\";
		char *name, *oldname;
		while ((oldname = strsep (&path, path_separators)) != NULL) {
			name = oldname;
		}

		names[count] = malloc (strlen (name) + 1);
		strncpy (names[count], name, strlen (name) + 1);

		// check for information correctness
		if (strcmp (full_path_names[count], "") == 0 || strcmp (name, "") == 0) {
			perror ("wrong service path/name format");
			return -1;
		}

		if (strcmp (proto, TCP_PROTO) != 0 && strcmp (proto, UDP_PROTO) != 0) {
			perror ("wrong protocol format");
			return -1;
		}

		char *endptr;
		strtol (port, &endptr, 10);

		if (!(strcmp (port, "") != 0 && strcmp (endptr, "") == 0)) {
			perror ("wrong port format");
			return -1;
		}

		if (strcmp (mode, WAIT_MODE) != 0 && strcmp (mode, NOWAIT_MODE) != 0) {
			perror ("wrong mode format");
			return -1;
		}

		// populating other infos
		protocols[count] = malloc (strlen (proto) + 1);
		ports[count] = malloc (strlen (port) + 1);
		modes[count] = malloc (strlen (mode) + 1);

		strncpy (protocols[count], proto, strlen (proto) + 1);
		strncpy (ports[count], port, strlen (port) + 1);
		strncpy (modes[count], mode, strlen (mode) + 1);
	}

	int services = count;
	printf ("services found: %d\n", services);

	for (int i = 0; i < services; i++) {
		printf("\t%s\n", names[i]);
	}

	return services;
}

int handle_request (service *serv, char **env) {
	// client communication sfd for TCP
	int comm_sfd = serv->sfd;

	// accept connection if TCP
	if (is_tcp (*serv)) {
		struct sockaddr_in client_addr;
		socklen_t cli_size = sizeof (client_addr);

		comm_sfd = accept (serv->sfd, (struct sockaddr *) &client_addr, &cli_size);
    	if (comm_sfd < 0) {
			perror ("error while accepting TCP connection");
			exit (EXIT_FAILURE);
		}
	}

	// fork process
	pid_t pid = fork ();

	if (pid < 0) {
		perror ("error while forking");
		return -1;
	} else if (pid == 0) {
		// child process

		// closing welcome socket if TCP
		if (is_tcp (*serv)) {
			close(serv->sfd);
		}

		// closing stdin, stdout, stderr
		close(0);
		close(1);
		close(2);

		// duplicating sfd (assigment to stdin, stdout and stderr is automatic since "dup" is searching
		// for the lowest fd not used)

		int newin = dup (comm_sfd);
		int newout = dup (comm_sfd);
		int newerr = dup (comm_sfd);

		if (newin != 0 || newout != 1 || newerr != 2) {
			exit (EXIT_FAILURE);
		}

		// executing service
		int err = execle (serv->path, serv->name, NULL, env);

		if (err < 0) {
			exit (EXIT_FAILURE);
		}

		exit (EXIT_SUCCESS);
	} else {
		// father process

		// close communication sfd if TCP
		if (is_tcp (*serv)) {
			close (comm_sfd);
		}

		// remove from select_set if service is in wait mode and register PID in data structure
		if (is_wait_mode(*serv)) {
			printf ("removing service from select set...\n");
			serv->pid = pid;
			FD_CLR (serv->sfd, &select_set);
		}
	}

	return 0;
}

int is_wait_mode (service serv) {
	return (strncmp (serv.mode, WAIT_MODE, strlen(WAIT_MODE)) == 0);
}

int is_tcp (service serv) {
	return strncmp(serv.transport_protocol,TCP_PROTO,TRANSPORT_PROTOCOL_LEN) == 0;
}
